def is_palindrome(a):
    """
    la fonction a pour but de detecter si un mot ou une phrase est un palindrome
    elle prend les caractere ecrite et les inverse et les compare pour detecter
    si dans les deux sens
    les mot s'ecrivent de la meme facon

    """




    for i in range(len(a)//2):
        if a[i] != a[-i-1]:
            return False
    return True
# Dictionnaire associatif des caractères accentuées en langue francophone
car = {
        "à" : "a",
        "ä" : "a",
        "â" : "a",
        "ç" : "c",
        "é" : "e",
        "è" : "e",
        "ë" : "e",
        "ï" : "i",
        "ô" : "o",
        "ù" : "u",
        "ü" : "u",
        "û" : "u",
        " " : "",
        "-" : "",
        "," : "",
        "'" : "",
        "?" : "",
        "!" : "",
        "." : ""
    }
# Remplacer des caractères par d'autres
def moulinette(a):
    a = a.lower()
    for cle,valeur in car.items():
        a = a.replace(cle,valeur)
    return a
# MAIN
a = moulinette(input("Entrez une phrase : "))
if is_palindrome(a) == True:
    print("C'est un palindrome.")
else:
    print("Ce n'est pas un palindrome.")